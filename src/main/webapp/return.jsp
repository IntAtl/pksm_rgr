<?xml version="1.0" encoding="UTF-8"?>
<%@page contentType="text/html; charset=UTF-8" import="java.util.Map" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Hello World!</title>
	<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8" />
	<link rel="stylesheet" type="text/css" href="consumer-servlet.css" />
</head>
<body>
	<div>
		<div>Login Success! - <a href="logout.jsp">Logout</a></div>
		<div>
			<fieldset>
				<legend>Your Login</legend>
				<input type="text" name="openid_identifier" value="${identifier}" />
			</fieldset>
		</div>
		<div>
			<fieldset>
				<legend>queryString</legend>
				<textarea name="queryString">${pageContext.request.queryString}</textarea>
			</fieldset>
		</div>
	</div>
</body>
</html>