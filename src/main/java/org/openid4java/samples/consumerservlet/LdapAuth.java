package org.openid4java.samples.consumerservlet;

import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

public class LdapAuth {
	private static final String LDAP_URL = "ldap://192.168.43.245:389";
	private static LdapAuth INSTANCE;
	private static String LDAP_DC = "dc=my,dc=ldap,dc=home";
	private static String LDAP_OU = "ou=users";
	private static String LDAP_CN = "cn=";
	private LdapAuth() {}
	
	public Boolean checkUser(String userName, String pass) {
		if (userName == null || userName.isEmpty() || userName.trim().isEmpty()) {
			return Boolean.FALSE;
		}
		Boolean res = Boolean.FALSE;
        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, LDAP_URL);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        StringBuilder sb = new StringBuilder();
        sb.append(LDAP_CN).append(userName).append(",");
        sb.append(LDAP_OU).append(",");
        sb.append(LDAP_DC);
        env.put(Context.SECURITY_PRINCIPAL, sb.toString());
        env.put(Context.SECURITY_CREDENTIALS, pass);

        DirContext ctx = null;
        NamingEnumeration results = null;
        try {
            ctx = new InitialDirContext(env);
            SearchControls controls = new SearchControls();
            controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            results = ctx.search("dc=my,dc=ldap,dc=home", "(objectclass=*)", controls);
            while (results.hasMore()) {
                SearchResult searchResult = (SearchResult) results.next();
                Attributes attributes = searchResult.getAttributes();
                System.out.println(" Person Common Name = " + attributes.get("cn"));
                System.out.println(" Person Display Name = " + attributes.get("displayName"));
                System.out.println(" Person logonhours = " + attributes.get("logonhours"));
                System.out.println(" Person MemberOf = " + attributes.get("memberOf"));
            }
            
            res = Boolean.TRUE;
        } catch (AuthenticationException e) {
			res = Boolean.FALSE;
		}
        catch (Throwable e) {
        	res = Boolean.FALSE;
            e.printStackTrace();
        } finally {
            if (results != null) {
                try {
                    results.close();
                } catch (Exception e) {
                }
            }
            if (ctx != null) {
                try {
                    ctx.close();
                } catch (Exception e) {
                }
            }
        }
		return res;
	}
	
	public static LdapAuth getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new LdapAuth();
		}
		return INSTANCE;
	}
}
